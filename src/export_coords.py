#!/usr/bin/env python
"""
export_coords - convert SVG path to SVG polyline /
                export path nodes to coordinates

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# standard library
import re

# compat
import six

# local library
import inkex
import cubicsuperpath
import simpletransform


try:
    inkex.localize()
except AttributeError:
    import gettext  # pylint: disable=wrong-import-order
    _ = gettext.gettext


__version__ = '0.1'


# Global "constants"
SVG_SHAPES = ('rect', 'circle', 'ellipse', 'line', 'polyline', 'polygon')
IDENT_MAT = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


# ----- import from path_lib.common

def is_group(node):
    """Check node for group tag."""
    return node.tag == inkex.addNS('g', 'svg')


def is_path(node):
    """Check node for path tag."""
    return node.tag == inkex.addNS('path', 'svg')


def is_basic_shape(node):
    """Check node for SVG basic shape tag."""
    return node.tag in (inkex.addNS(tag, 'svg') for tag in SVG_SHAPES)


def is_custom_shape(node):
    """Check node for Inkscape custom shape type."""
    return inkex.addNS('type', 'sodipodi') in node.attrib


def is_shape(node):
    """Check node for SVG basic shape tag or Inkscape custom shape type."""
    return is_basic_shape(node) or is_custom_shape(node)


def has_path_effect(node):
    """Check node for Inkscape path-effect attribute."""
    return inkex.addNS('path-effect', 'inkscape') in node.attrib


def is_modifiable_path(node):
    """Check node for editable path data."""
    return is_path(node) and not (has_path_effect(node) or
                                  is_custom_shape(node))

# ----- end import from path_lib.common


# ----- import from path_lib.geom

def nodelist_to_csp(alist):
    """Convert list of nodes into csp path."""
    csp = []
    if alist:
        csp.append([])  # create first sub-path
        for node in alist:
            csp[-1].append([])  # append empty node list
            for _ in range(3):
                csp[-1][-1].append([float(node[0]), float(node[1])])
    return csp


def csp_to_nodelist(csp, sub=0, nodes=None):
    """Convert csp into a list of nodes without control points."""
    if nodes is None:
        nodes = len(csp[sub])
    return [[(csp[1][0], csp[1][1]) for csp in subs]
            for subs in csp][sub][:nodes]

# ----- end import from path_lib.geom


# ----- import from path_lib.transform

def mat_compose_doublemat(mat1, mat2):
    """Compose two mats into a single one, return mat."""
    return simpletransform.composeTransform(mat1, mat2)


def mat_apply_to(mat, obj):
    """Call applyTransformTo{Path,Node} depending on obj type."""
    # pylint: disable=protected-access
    if isinstance(obj, list):
        simpletransform.applyTransformToPath(mat, obj)
    elif isinstance(obj, inkex.etree._Element):
        simpletransform.applyTransformToNode(mat, obj)


def mat_copy_from(node):
    """Return transformation matrix for node's preserved transform."""
    return simpletransform.parseTransform(node.get('transform'))


def mat_absolute(node):
    """Return transformation matrix transforming node to SVGRoot."""
    if node.getparent() is not None:
        return simpletransform.composeParents(node, IDENT_MAT)
    else:
        return mat_copy_from(node)


def mat_apply_copy_from(source, target):
    """Apply preserved transform from source to target."""
    mat = mat_copy_from(source)
    mat_apply_to(mat, target)

# ----- end import from path_lib.geom


# ----- start path2polyline

def formatted(f, precision=8):
    """Pretty-printer for floats, return formatted string."""
    # pylint: disable=invalid-name
    fstring = '.{0}f'.format(precision)
    return format(f, fstring).rstrip('0').rstrip('.')


def format_nodelist(alist, pr=8):
    """Format list of nodes as <polyline> 'points' value."""
    # pylint: disable=invalid-name
    return ' '.join([','.join([formatted(c, pr) for c in p]) for p in alist])


def is_closed(_):
    """NYI."""
    return False


def csp2cusp(csp):
    """Convert nodes to cusp nodes in csp path."""
    return nodelist_to_csp(csp_to_nodelist(csp))


def csp2poly(csp):
    """Create polyline based on csp path, return etree element"""
    if is_closed(csp[0]):
        path2poly = inkex.etree.Element(inkex.addNS('polygon', 'svg'))
    else:
        path2poly = inkex.etree.Element(inkex.addNS('polyline', 'svg'))
    path2poly.set('points', format_nodelist(csp_to_nodelist(csp)))
    return path2poly

# ----- end path2polyline


class EffectCompat(inkex.Effect):
    """inkex.Effect overload with updated document scale features."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

    # -----------------------------------------------------------------
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    #

    # Maintain separate dicts for 90 and 96 dpi to allow extensions to
    # implement support for legacy (or future) documents.
    __uuconv_90dpi = {
        'in': 90.0,
        'pt': 1.25,
        'px': 1.0,
        'mm': 3.543307086614174,
        'cm': 35.43307086614174,
        'm':  3543.307086614174,
        'km': 3543307.086614174,
        'pc': 15.0,
        'yd': 3240.0,
        'ft': 1080.0
    }

    __uuconv_96dpi = {
        'in': 96.0,
        'pt': 1.333333333333333,
        'px': 1.0,
        'mm': 3.779527559055119,
        'cm': 37.79527559055119,
        'm':  3779.527559055119,
        'km': 3779527.559055119,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0
    }

    # A dictionary of unit to user unit conversion factors.
    __uuconv = __uuconv_96dpi

    # New method: get_document_scale()
    # ================================
    # The new method supports arbitrary uniform scale factors and is
    # used in the unit conversion methods unittouu() and uutounit().

    def match_uuconv(self, val, eps=0.01):
        """Fuzzy matching of value to one of the known unit factors."""
        match = None
        for key in self.__uuconv:
            if inkex.are_near_relative(self.__uuconv[key], val, eps):
                match = self.__uuconv[key]
        return match or val

    def split_svg_length(self, string, percent=False):
        """Split SVG length string into float and unit string."""
        # pylint: disable=invalid-name
        param = unit = None
        if percent:
            unit_list = '|'.join(list(self.__uuconv.keys()) + ['%'])
        else:
            unit_list = '|'.join(list(self.__uuconv.keys()))
        param_re = re.compile(
            r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        unit_re = re.compile(
            '({0})$'.format(unit_list))
        if string is not None:
            p = param_re.match(string)
            u = unit_re.search(string)
            if p:
                try:
                    param = float(p.string[p.start():p.end()])
                except (KeyError, ValueError):
                    pass
            if u:
                try:
                    unit = u.string[u.start():u.end()]
                except KeyError:
                    pass
        return (param, unit)

    def get_page_dimension(self, attribute):
        """Retrieve value for SVGRoot attribute passed as argument.

        Return list of float and string.
        """
        string = self.document.getroot().get(attribute, "100%")
        val, unit = self.split_svg_length(string, percent=True)
        dimension = 100.0 if val is None else val
        dimension_unit = 'px' if unit is None else unit
        return (dimension, dimension_unit)

    def get_viewbox(self):
        """Retrieve value for viewBox from current document.

        Return list of 4 floats or None.
        """
        viewbox_attribute = self.document.getroot().get('viewBox', None)
        viewbox = []
        if viewbox_attribute:
            try:
                viewbox = list(float(i) for i in viewbox_attribute.split())
            except ValueError:
                pass
        if len(viewbox) != 4 or (viewbox[2] < 0 or viewbox[3] < 0):
            viewbox = None
        return viewbox

    def check_viewbox(self, width, w_unit, height, h_unit):
        """Verify values for SVGRoot viewBox width and height.

        Return list of 2 floats.
        """
        viewbox = self.get_viewbox()
        if viewbox is None:
            # If viewBox attribute is missing or invalid: calculate
            # viewBox dimensions corresponding to width, height
            # attributes. Treat '%' as special case, else assume 96dpi.
            vb_width = (width if w_unit == '%' else
                        self.__uuconv[w_unit] * width)
            vb_height = (height if h_unit == '%' else
                         self.__uuconv[h_unit] * height)
        else:
            vb_width, vb_height = viewbox[2:4]
        return (vb_width, vb_height)

    def get_aspectratio(self):
        """Get preserveAspectRatio from SVGRoot.

        Return list of 2 strings.
        """
        preserve_aspect_ratio = self.document.getroot().get(
            'preserveAspectRatio', "xMidYMid meet")
        try:
            return preserve_aspect_ratio.split()
        except ValueError:
            # Nothing to split: Set <meetOrSlice> parameter to default
            # 'meet'.  Note that the <meetOrSlice> parameter is ignored
            # if <align> parameter is 'none'.
            return (preserve_aspect_ratio, "meet")

    def get_aspectratio_scale(self, width, w_unit, height, h_unit,
                              vb_width, vb_height):
        """Calculate offset, scale based on SVGRoot preserveAspectRatio.

        Return list of 4 floats.
        """
        # pylint: disable=too-many-arguments
        # pylint: disable=too-many-locals
        x_offset = y_offset = 0.0
        scale = 1.0
        aspect_align, aspect_clip = self.get_aspectratio()
        if aspect_align == 'none':
            # TODO: implement support for non-uniform scaling
            # based on preserveAspectRatio attribute.
            pass
        else:
            width = vb_width * (width / 100.0) if w_unit == '%' else width
            height = vb_height * (height / 100.0) if h_unit == '%' else height
            vb_width = width if vb_width == 0 else vb_width
            vb_height = height if vb_width == 0 else vb_height
            scale_x = width / vb_width
            scale_y = height / vb_height
            # Force uniform scaling based on <meetOrSlice> parameter
            scale = (aspect_clip == "meet" and
                     min(scale_x, scale_y) or
                     max(scale_x, scale_y))
            # calculate offset based on <align> parameter
            align_x = aspect_align[1:4]
            align_y = aspect_align[5:8]
            offset_factor = {'Min': 0.0, 'Mid': 0.5, 'Max': 1.0}
            try:
                # TODO: verify units of calculated offsets
                x_offset = round(
                    offset_factor[align_x] * (width - vb_width*scale), 3)
                y_offset = round(
                    offset_factor[align_y] * (height - vb_height*scale), 3)
            except KeyError:
                pass
        return (x_offset, y_offset, scale, scale)

    def apply_viewbox(self):
        """Return offset (x, y) and scale for width, height of SVGRoot."""

        width, w_unit = self.get_page_dimension('width')
        height, h_unit = self.get_page_dimension('height')
        vb_width, vb_height = self.check_viewbox(
            width, w_unit, height, h_unit)
        x_offset, y_offset, scale_x, scale_y = self.get_aspectratio_scale(
            width, w_unit, height, h_unit, vb_width, vb_height)

        # Treat '%' as special case, else apply scale to conversion
        # factor from __uuconv.  Use match_uuconv() to allow precision
        # tolerance of the document's page dimensions.
        w_scale, h_scale = [(scale if unit == '%' else
                             self.match_uuconv(self.__uuconv[unit] * scale))
                            for unit, scale in (
                                (w_unit, scale_x), (h_unit, scale_y))]

        return (x_offset, y_offset, w_scale, h_scale)

    def get_document_scale(self):
        """Return document scale factor.

        Calculate scale based on these SVGRoot attributes:
        'width', 'height', 'viewBox', 'preserveAspectRatio'
        """
        unitfactor = self.__uuconv['px']  # fallback
        width_unitfactor, height_unitfactor = self.apply_viewbox()[2:4]
        return width_unitfactor or height_unitfactor or unitfactor

    # Unit conversion tools
    # =====================
    # Methods to assist unit handling in extensions / derived classes.

    def unittouu(self, string):
        """Return userunits given a string representation in units.

        Return float or None.
        """
        val = None
        if string is not None:
            val, unit = self.split_svg_length(string)
            if val is None:
                val = 0.0
            if unit is None:
                val /= self.get_document_scale()  # Assume default 'px'.
            elif unit in self.__uuconv.keys():
                val *= (self.__uuconv[unit] / self.get_document_scale())
        return val

    def uutounit(self, val, unit):
        """Return value in userunits converted to other units."""
        return val / (self.__uuconv[unit] / self.get_document_scale())

    def switch_uuconv(self, dpi):
        """Allow extensions to override internal resolution."""
        if dpi == '90':
            self.__uuconv = self.__uuconv_90dpi
        elif dpi == '96':
            self.__uuconv = self.__uuconv_96dpi
        else:  # unchanged
            pass

    #
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    # -----------------------------------------------------------------


class SelectedNodes(inkex.Effect):
    """inkex.Effect overload to implement processing of selected nodes."""

    def __init__(self):
        """Init base class if necessary."""
        if not hasattr(self, 'options'):
            inkex.Effect.__init__(self)

        # selected nodes passed from inkscape
        self.selected_nodes = {}

    # ----- import from future inkex.py

    def getselected_nodes(self):
        """Collect selected path nodes."""
        # TODO: add to Effect() class in inkex.py
        if hasattr(self.options, 'selected_nodes'):
            for path in self.options.selected_nodes:
                sel_data = path.rsplit(':', 2)
                path_id = sel_data[0]
                sub_path = int(sel_data[1])
                sel_node = int(sel_data[2])
                if path_id not in self.selected_nodes:
                    self.selected_nodes[path_id] = {sub_path: [sel_node]}
                else:
                    if sub_path not in self.selected_nodes[path_id]:
                        self.selected_nodes[path_id][sub_path] = [sel_node]
                    else:
                        self.selected_nodes[path_id][sub_path].extend(
                            [sel_node])
        else:
            self.selected_nodes = {}
            inkex.errormsg(_(
                "This version of Inkscape does not support processing " +
                "selected path nodes in script-based extensions."))

    # ----- end import from future inkex.py


class PathToPolyline(EffectCompat, SelectedNodes):
    """Class to convert paths to <polyline> or export node coords."""
    # pylint: disable=too-many-public-methods
    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        """Init base classes."""
        EffectCompat.__init__(self)
        SelectedNodes.__init__(self)

        # instance attributes
        self.valid = []
        self.skipped = []
        self.groups = []

        self.target_layer = self.__class__.__name__

        self.documentscale = None
        self.viewboxheight = None

        # global settings
        self.OptionParser.add_option("--verbose",
                                     action="store",
                                     type="inkbool",
                                     dest="verbose",
                                     default=True,
                                     help="Verbose mode")
        # tabs
        self.OptionParser.add_option("--tab",
                                     action="store",
                                     type="string",
                                     dest="tab",
                                     help="The selected UI-tab")
        # selection scope
        self.OptionParser.add_option("--recursive",
                                     action="store",
                                     type="inkbool",
                                     dest="recursive",
                                     default=False,
                                     help="Recurse into groups")
        self.OptionParser.add_option("--selected_nodes_only",
                                     action="store",
                                     type="inkbool",
                                     dest="selected_nodes_only",
                                     default=False,
                                     help="Selected nodes only")
        # convertor options
        self.OptionParser.add_option("--mode",
                                     action="store",
                                     type="string",
                                     dest="mode",
                                     default="insert_new",
                                     help="Mode")
        # path2poly options
        self.OptionParser.add_option("--copy_style",
                                     action="store",
                                     type="inkbool",
                                     dest="copy_style",
                                     default=False,
                                     help="Copy style from path to node")
        self.OptionParser.add_option("--export_points",
                                     action="store",
                                     type="inkbool",
                                     dest="export_points",
                                     default=False,
                                     help="Copy style from path to node")
        self.OptionParser.add_option("--relative_to",
                                     action="store",
                                     type="string",
                                     dest="relative_to",
                                     default="root",
                                     help="Coordinates relative to")
        self.OptionParser.add_option("--coord_system",
                                     action="store",
                                     type="string",
                                     dest="coord_system",
                                     default="svg",
                                     help="Coordinate system")
        self.OptionParser.add_option("--units",
                                     action="store",
                                     type="string",
                                     dest="units",
                                     default="uu",
                                     help="Units")
        self.OptionParser.add_option("--precision",
                                     action="store",
                                     type="int",
                                     dest="precision",
                                     default=3,
                                     help="Precision")
        self.OptionParser.add_option("--override_dpi",
                                     action="store",
                                     type="string",
                                     dest="override_dpi",
                                     default="default",
                                     help="Override internal dpi")

    # ----- import from path_lib.common

    def get_inkscape_version(self):
        """Return Inkscape version extracted from version attribute."""
        # NOTE: r14965 changed how this attribute is updated:
        # Inkscape no longer updates the attribute on load but on save.
        # New files thus don't have such an attribute anymore.
        # quick workaround: default to (assumed) 0.92
        inkscape_version_string = self.document.getroot().get(
            inkex.addNS('version', 'inkscape'), "0.92")
        version_match = re.compile(
            r'(0.48|0.48\+devel|0.91pre|0.91\+devel|0.91|0.92)')
        match_result = version_match.search(inkscape_version_string)
        if match_result is not None:
            inkscape_version = match_result.groups()[0]
        else:
            inkex.errormsg("Failed to retrieve Inkscape version.\n")
            inkscape_version = "Unknown"
        return inkscape_version

    def set_dpi(self, dpi):
        """Support 90 and 96 dpi."""
        if dpi == 'default':
            ink_ver = self.get_inkscape_version()
            # TODO: compare numerically to >= 0.92 for future releases
            if ink_ver == '0.91+devel' or ink_ver == '0.92':
                self.switch_uuconv('96')
            else:  # Assume older version with 90dpi
                self.switch_uuconv('90')
        else:
            self.switch_uuconv(dpi)
        # reset information about scale saved by overloaded methods
        self.documentscale = None
        self.viewboxheight = None

    # overload method from base class to store scale as instance attribute
    def get_document_scale(self):
        """Return document scale."""
        if self.documentscale is None:
            unitfactor = 1.0  # fallback 'px'
            width_unitfactor, height_unitfactor = self.apply_viewbox()[2:4]
            self.documentscale = (width_unitfactor or
                                  height_unitfactor or
                                  unitfactor)
        return self.documentscale

    def get_viewbox_height(self):
        """Return page height in SVG user units."""
        if self.viewboxheight is None:
            width, w_unit = self.get_page_dimension('width')
            height, h_unit = self.get_page_dimension('height')
            self.viewboxheight = self.check_viewbox(
                width, w_unit, height, h_unit)[1]
        return self.viewboxheight

    def create_layer(self, name):
        """Create custom layer with ID name, return etree element."""
        new_layer = inkex.etree.Element(inkex.addNS('g', 'svg'))
        new_layer.set('id', self.uniqueId(name))
        new_layer.set(inkex.addNS('groupmode', 'inkscape'), "layer")
        new_layer.set(inkex.addNS('label', 'inkscape'), name)
        self.document.getroot().append(new_layer)
        return new_layer

    def get_class_layer(self):
        """Return class layer with ID stored in self.target_layer."""
        class_layer = self.getElementById(self.target_layer)
        if class_layer is None or not is_group(class_layer):
            class_layer = self.create_layer(self.target_layer)
            self.target_layer = class_layer.get('id')
        return class_layer

    def report(self, mode="skipped"):
        """Report count of skipped (or valid) objects.

        Warn about empty selection.
        """
        total = len(self.valid) + len(self.skipped)
        if len(self.skipped):
            if mode == "skipped":
                if self.options.mode == "replace_d":
                    inkex.errormsg(_(
                        '{0} out of {1} objects '.format(
                            len(self.skipped), total) +
                        'in the selection have been skipped because ' +
                        'they are not paths or have a path effect applied.'))
                else:
                    inkex.errormsg(_(
                        '{0} out of {1} objects '.format(
                            len(self.skipped), total) +
                        'in the selection have been skipped because ' +
                        'they are not paths.'))
            if mode == "valid":
                if len(self.valid):
                    if self.options.mode == "replace_d":
                        inkex.errormsg(_(
                            '{0} out of {1} objects '.format(
                                len(self.valid), total) +
                            'in the selection have been modified.'))
                    else:
                        inkex.errormsg(_(
                            '{0} out of {1} objects '.format(
                                len(self.valid), total) +
                            'in the selection have been processed.'))
                else:
                    if self.options.mode == "replace_d":
                        inkex.errormsg(_(
                            'No modifiable paths found ' +
                            'among {0} objects '.format(total) +
                            'in the selection.'))
                    else:
                        inkex.errormsg(_(
                            'No paths found among {0} objects '.format(
                                total) +
                            'in the selection.'))
            if len(self.groups):
                inkex.errormsg(_(
                    '\nTotal count of groups processed: {0}'.format(
                        len(self.groups))))
        elif not total:
            inkex.errormsg(_(
                'This extension requires a selection of one or more paths.'))

    def recurse_selection(self, id_, node):
        """Recursively process selected objects and check object type."""
        if is_group(node) and self.options.recursive:
            self.groups.append(id_)
            for child in node:
                self.recurse_selection(child.get('id'), child)
        elif self.type_check(node):
            if self.options.selected_nodes_only:
                # filter paths based on selected nodes
                if id_ in self.selected_nodes.keys():
                    self.valid.append(id_)
                else:
                    self.skipped.append(id_)
            else:
                self.valid.append(id_)
        else:
            self.skipped.append(id_)

    def type_check(self, node):
        """Check type specific to sub-class PathToPolyline."""
        if self.options.mode == "replace_d":
            return is_modifiable_path(node)
        else:
            return is_path(node)

    def modify_path(self, node):
        """Generic method to be called by path-modifying extensions."""
        return self.convert_path(node)

    def effect(self):
        """Run initial loop for selected objects, report (optional)."""

        # support 90 and 96 dpi
        self.set_dpi(self.options.override_dpi)

        # parse args and build lists of nodes per subpath per path
        self.getselected_nodes()

        # main
        for id_, node in six.iteritems(self.selected):
            self.recurse_selection(id_, node)
        for id_ in self.valid:
            # run extension's effect for every valid object
            self.modify_path(self.getElementById(id_))
        if self.options.verbose:
            # output separator line for verbose report below pointlists
            if self.valid and (self.options.export_points or
                               self.options.tab == '"export"'):
                inkex.debug("\n=====\n")
            self.report()

    # ----- end import from path_lib.common

    def export_coordinates(self, node, csp):
        """Export node coordinates via stderr."""
        # Viewport
        if self.options.relative_to != 'node':
            # Apply preserved transforms of node.
            mat_apply_copy_from(node, csp)
        if self.options.relative_to == 'root':
            # Apply composed parent transforms of node's parent
            mat_apply_to(mat_absolute(node.getparent()), csp)

        # Coordinate system (useful with 'root'):
        if self.options.coord_system == 'desktop':
            # move vertically by negative viewBox height
            mat1 = simpletransform.parseTransform(
                'translate(0,-{0})'.format(self.get_viewbox_height()))
            # flip vertically
            mat2 = simpletransform.parseTransform('scale(1,-1)')
            mat_apply_to(mat_compose_doublemat(mat2, mat1), csp)

        # Document scale, output units:
        if self.options.units == 'css_px':
            scale = self.get_document_scale()
            mat1 = simpletransform.parseTransform('scale({0})'.format(scale))
            mat_apply_to(mat1, csp)
        elif self.options.units == 'display':
            doc_unit = self.getNamedView().get(
                inkex.addNS('document-units', 'inkscape'))
            if doc_unit is not None:
                scale = self.unittouu('1{0}'.format(doc_unit))
                mat1 = simpletransform.parseTransform(
                    'scale({0})'.format(1/scale))
                mat_apply_to(mat1, csp)
        elif self.options.units != 'uu':
            scale = self.unittouu('1{0}'.format(self.options.units))
            mat1 = simpletransform.parseTransform('scale({0})'.format(1/scale))
            mat_apply_to(mat1, csp)

        # send point coordinates to stderr, iterate over sub-paths
        for i in range(len(csp)):
            inkex.debug(
                format_nodelist(csp_to_nodelist(csp, i),
                                self.options.precision))

    def replace_d(self, node, csp):
        """Replace path data with cusp nodes from csp data."""
        # Convert nodes in csp path to cusp nodes with retracted handles.
        new_csp = []
        for subpath in csp:
            new_csp.append(csp2cusp([subpath])[0])
        node.set('d', cubicsuperpath.formatPath(new_csp))
        # Return point list via stderr.
        if self.options.export_points:
            inkex.debug(format_nodelist(csp_to_nodelist(new_csp)))

    def create_polyline(self, node, csp):
        """Create polyline element with path nodes as points."""
        # Compensate preserved transforms on node.
        mat_apply_copy_from(node, csp)
        # Get target layer to append/insert to.
        if self.options.mode == "insert_new":
            # Compensate node parent's transforms.
            mat_apply_to(mat_absolute(node.getparent()), csp)
            target_layer = self.get_class_layer()
        else:  # self.options.mode == "insert"; fallback for unknown
            target_layer = node.getparent()
        # Create polyline for each sub-path.
        for subpath in csp:
            new_poly = csp2poly([subpath])
            if self.options.mode == "replace_element":
                index = node.getparent().index(node)
                node.getparent().insert(index, new_poly)
            else:
                target_layer.append(new_poly)
            # Copy style from path to polyline.
            if self.options.copy_style:
                new_poly.set('style', node.get('style'))
            # Return point list via stderr.
            if self.options.export_points:
                inkex.debug(new_poly.get('points'))
        # Clean-up if replacing path element with polyline(s).
        if self.options.mode == "replace_element":
            node.getparent().remove(node)

    def convert_to_poly(self, node, csp):
        """Convert path nodes into cusp nodes or polyline points."""
        if self.options.mode == "replace_d":
            self.replace_d(node, csp)
        else:
            self.create_polyline(node, csp)

    def convert_path(self, node):
        """Parse path, convert to cusp nodes used as points for polyline."""

        # Convert path data to cubicsuperpath.
        csp = cubicsuperpath.parsePath(node.get('d'))

        if self.options.selected_nodes_only:
            # filter selected nodes from csp
            selnodes = self.selected_nodes[node.get('id')]
            temp_csp = []
            for sub_path in sorted(selnodes.keys()):
                temp_csp.append([])
                # inkex.debug(selnodes[sub_path])
                for point in sorted(selnodes[sub_path]):
                    temp_csp[-1].append(csp[sub_path][point])
            csp = temp_csp

        if self.options.tab == '"export"':
            self.export_coordinates(node, csp)
        elif self.options.tab == '"options"':
            self.convert_to_poly(node, csp)
        else:  # No action for help or unknown tab.
            pass


if __name__ == '__main__':
    ME = PathToPolyline()
    ME.affect()


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
